package homeWork_3_3.task2;

public interface RepairShop {
    boolean repair(String furniture);
}
