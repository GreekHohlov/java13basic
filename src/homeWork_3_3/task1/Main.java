package homeWork_3_3.task1;

public class Main{
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.getName();
        bat.eat();
        bat.sleep();
        bat.getAnimalClass();
        bat.wayOfBirth();
        bat.moveType();
        bat.moveSpeed();
        System.out.println("------------------");
        Dolphin dolphin = new Dolphin();
        dolphin.getName();
        dolphin.eat();
        dolphin.sleep();
        dolphin.getAnimalClass();
        dolphin.wayOfBirth();
        dolphin.moveType();
        dolphin.moveSpeed();
        System.out.println("------------------");
        Eagle eagle = new Eagle();
        eagle.getName();
        eagle.eat();
        eagle.sleep();
        eagle.getAnimalClass();
        eagle.wayOfBirth();
        eagle.moveType();
        eagle.moveSpeed();
        System.out.println("------------------");
        GoldFish goldFish = new GoldFish();
        goldFish.getName();
        goldFish.eat();
        goldFish.sleep();
        goldFish.getAnimalClass();
        goldFish.wayOfBirth();
        goldFish.moveType();
        goldFish.moveSpeed();
    }
}
