package homeWork_3_3.task4;

public class Dog {

    private String dogName;

    public Dog(String dogName){
        this.dogName = dogName;
    }
    public String toString(){
        return dogName;
    }
}
