package homeWork_3_3.task4;

import java.util.List;

public class GetWinner {
    public static void getWinners(List<Participant> participants, List<Dog> dogs, List<Integer> grades) {
        for (int i = 0; i < grades.size() - 1; i++) {
            Integer max = grades.get(i);
            int index = i;
            for (int j = i + 1; j < grades.size(); j++) {
                if (grades.get(j) > max) {
                    max = grades.get(j);
                    index = j;
                }
                int temp = grades.get(i);
                grades.set(i, grades.get(index));
                grades.set(index, temp);
                Participant participantTemp = participants.get(i);
                participants.set(i, participants.get(index));
                participants.set(index, participantTemp);
                Dog tempDog = dogs.get(i);
                dogs.set(i, dogs.get(index));
                dogs.set(index, tempDog);
            }
        }
        for (int i = 0; i < 3; i++) {
            System.out.println(participants.get(i).toString() + ": " + dogs.get(i).toString() +
                    ", " + ((int) ((grades.get(i) / 3.0) * 10) / 10.0));
        }
    }
}
