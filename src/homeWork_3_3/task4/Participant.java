package homeWork_3_3.task4;

public class Participant {

    private String name;


    public Participant(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
