package homeWork_3_3.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Participant> participants = new ArrayList<>();
        List<Integer> grade = new ArrayList<>();
        List<Dog> dogs = new ArrayList<>();
        System.out.print("Введите количество участников: ");
        int n = scanner.nextInt();
        System.out.println("Введите имя участника: ");
        for (int i = 0; i < n; i++) {
            participants.add(new Participant(scanner.next()));
            grade.add(0);
        }
        System.out.println("Введите клички собак: ");
        for (int i = 0; i < n; i++) {
            dogs.add(new Dog(scanner.next()));
        }
        System.out.println("Введите оценки для каждой собаки: ");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                grade.set(i, grade.get(i) + scanner.nextInt());
            }
        }
        GetWinner.getWinners(participants, dogs, grade);
    }
}
