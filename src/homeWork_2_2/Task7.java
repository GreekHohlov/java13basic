package homeWork_2_2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] name = new String[n];
        String[] nickName = new String[n];
        int[][] rating = new int[n][3];
        int indexTopOne = 0;
        int indexTopTwo = 0;
        int indexTopThree = 0;
        for (int i = 0; i < n; i++) {
            name[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            nickName[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                rating[i][j] = scanner.nextInt();
            }
        }
        double[] ratingTop = new double[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                ratingTop[i] += rating[i][j];
            }
            ratingTop[i] = (int)(ratingTop[i] / 3.0 * 10) / 10.0;
        }
        double max = ratingTop[0];
        for (int i = 1; i < n; i++) {
            if (ratingTop[i] > max){
                indexTopOne = i;
            }
        }
        for (int i = 1; i < n; i++) {
            if (ratingTop[i] > max && ratingTop[i] < ratingTop[indexTopOne]){
                indexTopTwo = i;
            }
        }
        for (int i = 1; i < n; i++) {
            if (ratingTop[i] > max && ratingTop[i] < ratingTop[indexTopOne] && ratingTop[i] < ratingTop[indexTopTwo]){
                indexTopThree = i;
            }
        }
        System.out.println(name[indexTopOne] + ": " + nickName[indexTopOne] + ", " + ratingTop[indexTopOne]);
        System.out.println(name[indexTopTwo] + ": " + nickName[indexTopTwo] + ", " + ratingTop[indexTopTwo]);
        System.out.println(name[indexTopThree] + ": " + nickName[indexTopThree] + ", " + ratingTop[indexTopThree]);

    }
}
