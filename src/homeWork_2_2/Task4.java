package homeWork_2_2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];
        int[][] newMatrix = new int[n - 1][n - 1];
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int pI = 0;
        int pJ = 0;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
               if (matrix[i][j] == p){
                   pI = i;
                   pJ = j;
               }
            }
        }
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                if (i < pI && j < pJ){
                newMatrix[i][j] = matrix[i][j];
                }
                if (i < pI && j > pJ){
                    newMatrix[i][j - 1] = matrix[i][j];
                }
                if (i > pI && j < pJ){
                    newMatrix[i - 1][j] = matrix[i][j];
                }
                if (i > pI && j > pJ){
                    newMatrix[i - 1][j - 1] = matrix[i][j];
                }
            }
        }
        for (int i = 0; i < n - 1; i++){
            System.out.print(newMatrix[i][0]);
            for (int j = 1; j < n - 1; j++){
                System.out.print(" " + newMatrix[i][j]);
            }
            System.out.println();
        }
    }
}
