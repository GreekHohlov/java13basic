package homeWork_2_2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] table = new int[8][4];
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int k = scanner.nextInt();
        int[] sum = new int[4];
        boolean flag = true;
        table[0][0] = a;
        table[0][1] = b;
        table[0][2] = c;
        table[0][3] = k;
        for (int i = 1; i < 8; i++){
            for (int j = 0; j < 4; j++){
                table[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 8; j++) {
                sum[i] += table[j][i];
            }
        }
        for (int i = 0; i < 4; i++){
            if (sum[i] >= table[0][i]){
                flag = false;
                break;
            }
        }
        if (flag){
            System.out.println("Отлично");
        }
        else {
            System.out.println("Нужно есть поменьше");
        }

    }
}
