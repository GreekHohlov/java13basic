package homeWork_2_2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];
        boolean equal = true;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < n - 1; i++){
            for (int j = 0; j < n - 1; j++){
                if (matrix[i][j] != matrix[n - 1 - j][n - 1 - i]){
                    equal = false;
                    break;
                }
            }
        }
        if (equal){
            System.out.println(equal);
        }
        else{
            System.out.println(equal);
        }
    }
}
