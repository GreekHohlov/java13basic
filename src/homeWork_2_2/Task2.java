package homeWork_2_2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = x1, j = y1; i <= x2; i++){
            matrix[j][i] = 1;
        }
        for (int i = y2, j = x1; j <= x2; j++){
            matrix[i][j] = 1;
        }
        for (int i = y1, j = x1; i <= y2; i++){
            matrix[i][j] = 1;
        }
        for (int i = y1, j = x2; i <= y2; i++){
            matrix[i][j] = 1;
        }
        for (int i = 0; i < n; i++) {
            System.out.print(matrix[i][0]);
            for (int j = 1; j < n; j++) {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
    }
}
