package homeWork_2_2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(extractNumber(n));
    }

    private static String extractNumber(int n) {
        String number = "";
        if (n < 10){
            return number += n;
        }
        else {
            return number += n % 10 + " " + extractNumber(n / 10);
        }
    }
}
