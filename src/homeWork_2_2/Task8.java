package homeWork_2_2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(printNumber(n));
    }

    private static int printNumber(int n) {
        int sum = 0;
        if (n < 10){
            return n;
        }
        else{
            return sum += n % 10 + printNumber(n / 10);
        }
    }
}
