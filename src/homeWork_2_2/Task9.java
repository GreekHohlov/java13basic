package homeWork_2_2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int number = 0;
        do {
            number *= 10;
            number += n % 10;
            n /= 10;
        }while (n > 0);

        System.out.println(extractNumber(number));
    }

    private static String extractNumber(int n) {
        String number = "";
        if (n < 10){
            return number += n;
        }
        else {
            return number += n % 10 + " " + extractNumber(n / 10);
        }
    }
}
