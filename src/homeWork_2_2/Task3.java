package homeWork_2_2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        String[][] matrix = new String[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int x1 = i + 2;
                int y1 = j + 1;
                int x2 = i + 2;
                int y2 = j - 1;
                int x3 = i + 1;
                int y3 = j + 2;
                int x4 = i + 1;
                int y4 = j - 2;
                int x5 = i - 1;
                int y5 = j + 2;
                int x6 = i - 1;
                int y6 = j - 2;
                int x7 = i - 2;
                int y7 = j + 1;
                int x8 = i - 2;
                int y8 = j - 1;
                if ((x1 == x && y1 == y) || (x2 == x && y2 == y) || (x3 == x && y3 == y) || (x4 == x && y4 == y)
                        || (x5 == x && y5 == y) || (x6 == x && y6 == y) || (x7 == x && y7 == y) || (x8 == x && y8 == y)){
                    matrix[j][i] = "X";
                }
                else {
                    matrix[j][i] = "0";
                }
            }
        }
        matrix[y][x] = "K";
        for (int i = 0; i < n; i++) {
            System.out.print(matrix[i][0]);
            for (int j = 1; j < n; j++) {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
    }
}