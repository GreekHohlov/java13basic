package homeWork_2_2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] array1 = new int[m][n];
        int[] array2 = new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                array1[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            int minValue = array1[i][0];
            for (int j = 1; j < n; j++) {
                if (minValue > array1[i][j]){
                    minValue = array1[i][j];
                }
            }
            array2[i] = minValue;
        }
        System.out.print(array2[0]);
        for (int i = 1; i < m; i++){
            System.out.print(" " + array2[i]);
        }
    }
}
