package homeWork_3_2;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();

        library.addBook("Джон Толкин", "Властелин колец");
        library.addBook("Лев Толстой", "Война и мир");
        library.addBook("Лев Толстой", "Анна Каренина");
        library.addBook("Фёдор Достоевский", "Преступление и наказание");
        library.addBook("Михаил Булгаков", "Мастер и Маргарита");
        library.addBook("Алан Милн", "Винни-Пух");
        library.addBook("Михаил Булгаков", "Собачье сердце");
        library.addBook("Лев Толстой", "Детство. Отрочество. Юность.");
        library.addBook("Лев Толстой", "Кавказский пленник");
        library.addBook("Антон Чехов", "Три сестры");
        library.addBook("Аркадий и Борис Стругацкие", "Обитаемый остров");
        library.addBook("Николай Носов", "Незнайка на Луне");
        library.addBook("Аркадий и Борис Стругацкие", "Понедельник начинается в субботу");
        library.addBook("Николай Гоголь", "Петербургские повести");
        System.out.println("----------------");

        System.out.println("Добавление уже существующей книги");
        library.addBook("Антон Чехов", "Три сестры");
        System.out.println("----------------");

        System.out.println("Запрос книги по названию");
        library.getBook("Петербургские повести");
        System.out.println("----------------");

        System.out.println("Получение списка книг по автору");
        library.getBookByAuthor("Лев Толстой");
        System.out.println("----------------");

        System.out.println("Удаление книги по названию");
        library.removeBook("Три сестры");
        System.out.println("----------------");
        System.out.println("----------------");

        System.out.println("Добавление посетителей библиотеки");
        library.createVisitors("Власов Станислав");
        library.createVisitors("Зайцев Осип");
        library.createVisitors("Горбачев Дмитрий");
        library.createVisitors("Ларионов Егор");
        library.createVisitors("Фомичев Феликс");
        System.out.println("----------------");

        System.out.println("Выдача книги посетителю");
        library.giveBook("Фомичев Феликс", "Николай Носов Незнайка на Луне");
        System.out.println("----------------");

        System.out.println("Попытка выдачи книги посетителю, который уже взял книгу, но еще не вернул");
        library.giveBook("Фомичев Феликс", "Лев Толстой Кавказский пленник");
        System.out.println("----------------");

        System.out.println("Попытка выдачи книги, которая уже выдана на руки другому посетителю");
        library.giveBook("Горбачев Дмитрий", "Николай Носов Незнайка на Луне");
        System.out.println("----------------");

        System.out.println("Удаление книги, которая выдана посетителю");
        library.removeBook("Николай Носов Незнайка на Луне");
        System.out.println("----------------");

        System.out.println("Попытка вернуть книгу, выданную другому посетителю");
        library.returnBook("Горбачев Дмитрий", "Николай Носов Незнайка на Луне", 5.0);
        System.out.println("----------------");

        System.out.println("Посетитель возвращает книгу");
        library.returnBook("Фомичев Феликс", "Николай Носов Незнайка на Луне", 3.0);
        System.out.println("----------------");

        System.out.println("Посетитель, вернувший книгу, берет другую");
        library.giveBook("Фомичев Феликс", "Лев Толстой Кавказский пленник");
        System.out.println("----------------");

        System.out.println("Посетитель берет книгу, которая была выдана ранее и была возвращена");
        library.giveBook("Горбачев Дмитрий", "Николай Носов Незнайка на Луне");
        library.returnBook("Горбачев Дмитрий", "Николай Носов Незнайка на Луне", 5.0);
        System.out.println("----------------");

        System.out.println("Выдача и возврат книги \"Николай Носов Незнайка на Луне посетителям\" \nДля тестирования" +
                "метода оценивания книг при возврате книг");
        library.giveBook("Зайцев Осип", "Николай Носов Незнайка на Луне");
        library.returnBook("Зайцев Осип", "Николай Носов Незнайка на Луне", 6.0);
        library.giveBook("Ларионов Егор", "Николай Носов Незнайка на Луне");
        library.returnBook("Ларионов Егор", "Николай Носов Незнайка на Луне", 5.0);
        library.giveBook("Власов Станислав", "Николай Носов Незнайка на Луне");
        library.returnBook("Власов Станислав", "Николай Носов Незнайка на Луне", 4.0);
        System.out.println("----------------");

        System.out.println("Получить оценку книги");
        System.out.println(library.getGrades("Николай Носов Незнайка на Луне"));
        System.out.println("----------------");
    }
}