package homeWork_3_2;


public class Book {
    private String author;
    private String title;
    private String book;

    public Book() {
        this.book = "";
    }
    public Book(String author, String title) {
        this.author = author;
        this.title = title;
        this.book = this.author + " " + this.title;
    }

    public String toString() {
        return book;
    }
}
