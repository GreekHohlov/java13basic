package homeWork_3_2;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private Book book;
    private Visitor visitor;
    private int id = 1;
    //Список книг, которые есть в библиотеке.
    private final List<Book> books = new ArrayList<Book>();
    //Список переменных типа boolean. Индекс переменной совпадает с индексом книги в List books.
    //Используется для определения состояния книги.
    //При значении false книга в наличии, при false - книга выдана посетителю.
    private final List<Boolean> outStocks = new ArrayList<>();
    //Список посетителей библиотеки.
    private final List<Visitor> visitors = new ArrayList<>();
    //Список id посетителей библиотки, идентификаторов читателей, которые запросили книгу.
    //Индекс id совпадает с индексом посетителя из List visitors.
    private final List<Integer> idList = new ArrayList<Integer>();
    //Список переменных типа boolean. Индекс переменной совпадает с индексом посетителя в List visitors.
    //Используется для определения, одолжил ли посетитель книгу в библиотеке.
    //При значении true книга выдана, при false - книга не выдана.
    private final List<Boolean> takeBook = new ArrayList<>();
    //Список переменных типа Integer.
    //Используется для определения, какой посетитель какую книгу одолжил.
    //Номер индекса совпадает с индексом посетителя, значение в этом индексе совпадает с индексом книги.
    private final List<Integer> takeBookId = new ArrayList<>();
    //Список переменных типа Double.
    //В элементе с индексом, совпадающим с индексом из List book суммируются все оценки поставленные книге в момент
    //возврата книги
    private final List<Double> sumGrades = new ArrayList<>();
    //Список переменных типа Integer.
    //Используется для хранения количества оценок поставленных книге. Индекс, содержаший колличество оценок совпадаетс
    //с индексом книги из List book.
    private final List<Integer> numbersOfGrades = new ArrayList<>();

    /**
     * Метод передает полученные данные в класс Book, от котороего получает наименование книги. Если такая книга еще не
     * существует в списке books, то в список добавляется новый элемент с наименованием книги.
     * В список одолженных книг добавляется элемент с значением false, кнаги не одолжена.
     * В список оценок книг добавляется элемент с значением 0.0, так как книгу еще не одалживали и не добавляли оценок
     * при возврате.
     * В список количества оценок добавляется элемент с значением 0, так как оценок еще нет.
     * В консоль выводится сообщение о добавлении книги с отображением её наименования.
     * Если такая книга уже есть в списке, то выводится сообщение о её наличии.
     *
     * @param author имя и фамилия автора книги.
     * @param book   Название книги.
     */
    public void addBook(String author, String book) {
        this.book = new Book(author, book);
        if (!books.toString().contains(this.book.toString())) {
            books.add(this.book);
            outStocks.add(false);
            sumGrades.add(0.0);
            numbersOfGrades.add(0);
            System.out.println("Книга \"" + this.book.toString() + "\" добавлена в библиотеку.");
        } else {
            System.out.println("Книга \"" + author + " " + book + "\" уже есть в библиотеке.");
        }
    }

    /**
     * Из списка книг через метод getBookIndex() запрашивается индекс книги в списке. На основании этого индекса
     * проверяется статус книги (существует/в наличии/одолжена), если книга существует и не одолжена,
     * списке в значение индекса книги устанавливается значение "", чтобы не происходило смещения элементов
     * при удлалении книги из списков.
     * При статусе книги одолжена/не существует выдается соответствующее сообщение, книга не удаляется.
     *
     * @param book наименование книги.
     */
    public void removeBook(String book) {
        int bookIndex = getBookIndex(book);
        if (bookIndex >= 0 && !outStocks.get(bookIndex)) {
            System.out.println("Книга \"" + books.get(bookIndex) + "\" удалена из библиотеки.");
            books.set(bookIndex, new Book());
        } else if (bookIndex >= 0 && outStocks.get(bookIndex)) {
            System.out.println("Книга \"" + books.get(bookIndex) + "\" " +
                    "выдана посетителю и не может быть удалена!");
        } else if (bookIndex < 0) {
            System.out.println("Книга с названием \"" + book + "\" не существует в библиотеке.");
        }
    }

    /**
     * В методе идет обращение к методу getBookIndex() для получения индекса книги в списке книг и выводится в консоль
     * значение соответствующего индекса.
     *
     * @param book наименование книги
     */
    public void getBook(String book) {
        int bookIndex = getBookIndex(book);
        if (bookIndex >= 0) {
            System.out.println(books.get(bookIndex));
        } else {
            System.out.println("Книга с названием " + book + " не существует в библиотеке.");
        }
    }

    /**
     * В методе с исползованием цикла fori происходит перебор элементов списка книг, если елемент содержит такие имя и
     * фамилию, то данныей элемент выводится в консоль.
     *
     * @param author имя и фамилия автора.
     */
    public void getBookByAuthor(String author) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).toString().contains(author)) {
                System.out.println(books.get(i));
            }
        }
    }

    /**
     * В методе создается посетитель библиотки через обращение к классу Visitors.
     * Новый посетитель добавляется в список. В списко idList добаляется значение null, так как посетитель еще не
     * одалживал книг и у него нет читательского номера.
     * В список takeBook добавляется элемент с значенем false, так как книга в текущий момент не одолжена
     * В список takeBookId добавляется злемент с значением -1, в момент получения книги в это индекс будет записан
     * индекс взятой книги.
     *
     * @param visitor имя и фамилия посетителя.
     */
    public void createVisitors(String visitor) {
        this.visitor = new Visitor(visitor);
        visitors.add(this.visitor);
        idList.add(Visitor.id);
        takeBook.add(false);
        takeBookId.add(-1);
    }

    /**
     * Метод полчает индекс посетителя из списка visitors с помощью метода etVisitorIndex() и возвращает значение
     * элемента из списка visitors с этим индексом.
     *
     * @param visitor имя и фамилия посетителя
     * @return имя и фамилия посетителя.
     */
    public String getVisitors(String visitor) {
        int visitorIndex = getVisitorIndex(visitor);
        return visitors.get(visitorIndex).toString();
    }

    /**
     * Метод выдачи книг посетителям. Метод полчает индекс посетителя из списка visitors с помощью
     * метода etVisitorIndex(), если такого посетителя ранее не было, то идет обращение к методу создания посетителя.
     * После создания нового посетителя выполняется поиск запрощенной и получение книги через метод getBookIndex().
     * Если книги не существуте, то выводится соответствующее сообщение.
     * При наличии книги выполняется проверка, не одолжена ли она, если кнаги в библиотке и посетитель обращается
     * впервые, то ему присваеватеся читательский номер и выдается книга. В соответсвующие списки вносятся данные об
     * индексе взятой книге и индексе посетителя, взявшего эту книгу.
     * Если у посетителя уже есть книга на руках, то выдастся соответствующее сообщение.
     *
     * @param visitor имя и фамилия посетителя.
     * @param book    наименование книги.
     */
    public void giveBook(String visitor, String book) {
        int visitorIndex = getVisitorIndex(visitor);
        if (visitorIndex < 0) {
            createVisitors(visitor);
            giveBook(getVisitors(visitor), book);
        }
        int bookIndex = getBookIndex(book);
        if (bookIndex >= 0 && !outStocks.get(bookIndex)) {
            if (!takeBook.get(visitorIndex)) {
                if (idList.get(visitorIndex) == null) {
                    idList.set(visitorIndex, id);
                    id++;
                }
                takeBook.add(visitorIndex, true);
                outStocks.add(bookIndex, true);
                takeBookId.set(visitorIndex, bookIndex);
                System.out.println("Книга \"" + books.get(bookIndex) + "\" выдана " + visitors.get(visitorIndex));
            } else {
                System.out.println("Посетителю " + visitors.get(visitorIndex) + " уже выдана ранее книга!");
            }
        } else if (bookIndex >= 0 && outStocks.get(bookIndex)) {
            if (idList.get(visitorIndex) == null) {
                idList.set(visitorIndex, id);
                id++;
                System.out.println("Книга \"" + books.get(bookIndex) + "\" выдана другому посетителю.");
            } else {
                System.out.println("Книга \"" + books.get(bookIndex) + "\" выдана другому посетителю.");
            }
        } else {
            System.out.println("Книга \"" + book + "\" не существует в библиотеке.");
        }
    }

    /**
     * Метод получет индекс посетителя из списка Visitors через метод getVisitorIndex() и индекс возвращаемой книги
     * через метод getBookIndex(), на основе этих индексов выполняется проверка, этот ли посетитель брал книгу, если нет,
     * то возврат невозможен. Если книга взята им, то в соответствующих списках устанавливаются значения, что у
     * посетителя нет книги на руках и соответствующая книга находится в библиоткет.
     * Так же при возврате книги посетитель должен поставить оценку возвращаемой книге.
     * Значение оценки суммируется с значенем из списка sumGrades с помощью метода addGrades().
     *
     * @param visitor имя и фамилия посетителя.
     * @param book    наименование книги.
     * @param grades  оценка возвращаемой книги.
     */
    public void returnBook(String visitor, String book, Double grades) {
        int visitorIndex = getVisitorIndex(visitor);
        int bookIndex = getBookIndex(book);
        if (bookIndex >= 0) {
            if (takeBookId.get(visitorIndex) == bookIndex) {
                System.out.println(visitor + " вернул книгу \"" + books.get(bookIndex) + "\"");
                outStocks.set(bookIndex, false);
                takeBook.set(visitorIndex, false);
                takeBookId.set(visitorIndex, -1);
                addGrades(book, grades);
            } else {
                System.out.println("Книга \"" + books.get(bookIndex) + "\" выдана другому читателю и не может быть" +
                        " возвращена " + visitors.get(visitorIndex) + "!");
            }
        } else {
            System.out.println("Книга \"" + book + "\" не существует в библиотеке.");
        }

    }

    /**
     * Метод получает идекс оцениваемой книги с помощью метода getBookIndex(). На основе этого индекса в списоке
     * sumGrades элемент с этим индексом суммируется с полученной оценкой из метода возврата книги,
     * в списке numbersOfGrades элемент с этим идексом увеличивается на 1.
     *
     * @param book   наименование книги.
     * @param grades оценка.
     */
    public void addGrades(String book, Double grades) {
        int bookIndex = getBookIndex(book);
        sumGrades.set(bookIndex, sumGrades.get(bookIndex) + grades);
        numbersOfGrades.set(bookIndex, numbersOfGrades.get(bookIndex) + 1);
    }

    /**
     * Метод получает идекс для запрашиваемой книги с помощью метода getBookIndex() и на основе этого индекса значение
     * списка sumGrades делит на значение элемента этого индекса из списка numbersOfGrades с окрурглением до сотых и
     * возвращает полученное значение.
     *
     * @param book наименование книги.
     * @return среднеарифметицеское оценок книги.
     */
    public double getGrades(String book) {
        int bookIndex = getBookIndex(book);
        return (int) ((sumGrades.get(bookIndex) / numbersOfGrades.get(bookIndex)) * 100) / 100.0;
    }

    /**
     * Метод в цикле fori сравнивает каждый элемент списка с посетителем, если есть совпадение - выполнение цикла
     * прерывается, переменной visitorIndex присваевается значение индекса, по которому найден посетитель.
     *
     * @param visitor имя и фамилия посетителя.
     * @return индекс посетителя, либо -1, если посетитель не найден.
     */
    public int getVisitorIndex(String visitor) {
        int visitorIndex = -1;
        for (int i = 0; i < visitors.size(); i++) {
            if (visitors.get(i).toString().contains(visitor)) {
                visitorIndex = i;
                break;
            }
        }
        return visitorIndex;
    }

    /**
     * Метод в цикле fori сравнивает каждый элемент списка с наименованием книги, если есть совпадение - выполнение цикла
     * прерывается, переменной bookIndex присваевается значение индекса, по которому найдена книга.
     *
     * @param book наименование книги.
     * @return индекс книги, либо -1, если книга не найдена.
     */
    public int getBookIndex(String book) {
        int bookIndex = -1;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).toString().contains(book)) {
                bookIndex = i;
                break;
            }
        }
        return bookIndex;
    }
}
