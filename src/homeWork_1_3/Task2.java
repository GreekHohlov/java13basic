package homeWork_1_3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int sum = 0;
        if(m > 0 && n > 0 && n > m){
            while (m <= n){
                sum += m;
                m++;
            }
            System.out.println(sum);
        }
    }
}
