package homeWork_1_3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int sum = 0;
        if(m > 0 && n > 0 && n < 10){
            for (int i = 1; i <= n; i++){
                sum += Math.pow(m, i);
            }
            System.out.println(sum);
        }
    }
}
