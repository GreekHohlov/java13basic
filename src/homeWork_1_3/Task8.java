package homeWork_1_3;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int p = input.nextInt();
        int[] a = new int[n];
        if (p > 0 && p < 1000 && n > 0 && n < 1000) {
            for (int i = 0; i < n; i++) {
                a[i] = input.nextInt();
                if (a[i] < 0 && a[i] > 1000 ){
                    i--;
                }
            }
        }
        System.out.println(sum(a, p));
    }

    public static int sum(int[] a, int p) {
        int result = 0;
        for (int i = 0; i < a.length; i++){
            if (a[i] > p){
                result += a[i];
            }
        }
        return result;
    }
}
