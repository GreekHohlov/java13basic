package homeWork_1_3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        if (n > 2 && n <10) {
            for (int i = 1; i <= n; i++) {
                for (int j = n; j > i; j--) {
                    System.out.print(" ");
                }
                for (int k = 0; k < i; k++) {
                    System.out.print("#");
                }
                for (int k = 1; k < i; k++) {
                    System.out.print("#");
                }
                System.out.println();
            }
            for (int i = 1; i < n; i++) {
                System.out.print(" ");
            }
            System.out.print("|");
        }
    }
}
