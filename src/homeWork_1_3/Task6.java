package homeWork_1_3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        if (n> 0 && n < 1000000){
            System.out.print(n / 8 + " " + n % 8 / 4 + " " + n % 8 % 4 / 2 + " " + n % 8 % 4 % 2);
        }
    }
}
