package homeWork_1_3;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        if(n > 0 && n < 1000000){
            if (n % 10 != 0){
                endNoZero(n);
            }
            else{
                endZero(n);
            }
        }
    }

    public static void endZero(int n) {
        while (n > 0){
            int lenghtN = String.valueOf(n).length() - 1;
            System.out.println(n / (int)Math.pow(10, lenghtN));
            n = n - n / (int)Math.pow(10, lenghtN) * (int)Math.pow(10, lenghtN);
        }
        System.out.println(0);
    }

    public static void endNoZero(int n) {
        while (n > 0){
            int lenghtN = String.valueOf(n).length() - 1;
            System.out.println(n / (int)Math.pow(10, lenghtN));
            n = n - n / (int)Math.pow(10, lenghtN) * (int)Math.pow(10, lenghtN);
        }
    }
}
