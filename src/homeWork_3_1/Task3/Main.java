package homeWork_3_1.Task3;
import homeWork_3_1.Task2.Student;
public class Main {
    public static void main(String[] args) {
        //Создать массив объектов Student
        Student[] student = new Student[5];
        //Создать обекты Student
        Student student0 = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        Student student4 = new Student();
        //Заполнить массив
        student[0] = student0;
        student0.setName("Петр");
        student0.setSurname("Васильев");
        student0.setGrades(new int[] {4, 4, 1, 3, 4, 1, 4, 9, 6, 5});

        student[1] = student1;
        student1.setName("Иван");
        student1.setSurname("Гаврилов");
        student1.setGrades(new int[] {5, 5, 2, 4, 1, 4, 8, 5, 1, 3});

        student[2] = student2;
        student2.setName("Семен");
        student2.setSurname("Тарасов");
        student2.setGrades(new int[] {5, 1, 5, 3, 7, 4, 3, 5, 4, 7});

        student[3] = student3;
        student3.setName("Александр");
        student3.setSurname("Климов");
        student3.setGrades(new int[] {8, 4, 3, 1, 7, 4, 1, 3, 3, 5});

        student[4] = student4;
        student4.setName("Глеб");
        student4.setSurname("Терентьев");
        student4.setGrades(new int[] {1, 6, 1, 4, 2, 6, 3, 1, 1, 7});


        //Получить данные по лучшему студенту
        Student bestStudents = StudentService.bestStudent(student);
        System.out.println("Лучший студент: " + bestStudents.getName() + " " + bestStudents.getSurname() +
                "\nСредний балл: " + bestStudents.getAverage());
        //Вывести на печать отсортированный по фамилиям массив студентов
        StudentService.sortByName(student);
    }
}
