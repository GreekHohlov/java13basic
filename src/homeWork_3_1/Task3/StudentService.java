package homeWork_3_1.Task3;

import homeWork_3_1.Task2.Student;
/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */
import java.util.ArrayList;

public class StudentService {
    private Student[] student;

    private StudentService() {

    }

    /**
     * Метод получает массив студентов, через цикл, находит наибольший средний балл и возвращает студнета с этим средним
     * баллом.
     * @param student массив студентов.
     * @return студент с наибольшим средним баллом.
     */
    public static Student bestStudent(Student[] student) {
        double maxAverage = 0;
        int k = 0;
        for (int i = 0; i < student.length; i++) {
            if (student[i].getAverage() > maxAverage){
                maxAverage = student[i].getAverage();
                k = i;
            }
        }
        return student[k];
    }

    /**
     * Метод получает массив студентов, записывает его в ArrayList, выполняет сортивроку по алфавиту и передает данные
     * в новый массив String, после чего выводит на печать фамилии студентов.
     * @param student массив студентов.
     */
    public static void sortByName(Student[] student) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < student.length; i++) {
            arrayList.add(i, student[i].getSurname());
        }
        arrayList.sort(null);
        String[] sortArray = arrayList.toArray(new String[0]);
        System.out.println("Список студентов в алфавитном порядке:");
        for (int i = 0; i < sortArray.length; i++) {
            System.out.println(sortArray[i]);
        }
    }
}
