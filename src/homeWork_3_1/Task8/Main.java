package homeWork_3_1.Task8;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm("dollars", 61);
        Atm atm2 = new Atm("rubles", 63);
        //Конвертировать доллары в рубли
        atm1.convertDollars(89);
        //Конвертировать рубли в доллары
        atm2.convertRubles(89);
        //Количество обращений к классу
        System.out.println("Количество обращений к классу " + Atm.getNumberOfUsage());
    }
}
