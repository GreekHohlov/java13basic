package homeWork_3_1.Task8;

public class Atm {
    private double rublesToDollars;
    private double dollarsToRubles;
    private static int numbersOfUsage = 0;

    /**
     * В конструктор передается валюта для конвертации и курс этой валюты относительно другой.
     * Увеличивается на 1 количество обращений к классу.
     * @param currency валюта для конвертации: dollars, rubles.
     * @param ratio курс конвертации выбранной валюты.
     */
    public Atm(String currency, double ratio){
        if (currency.equals("dollars")){
            this.dollarsToRubles = ratio;
        }else if (currency.equals("rubles")){
            this.rublesToDollars = ratio;
        } else {
            System.out.println("Введен недопустимая валюта для конвертации");
        }
        numbersOfUsage++;
    }

    /**
     * Метод получает количество рублей для конвертации и конвертирует по установленному курсу и вывадит полученное
     *      * количество рублей на печать.
     * Увеличивается на 1 количество обращений к классу.
     * @param roubles количество конвертируемых рублей.
     */
    public void convertRubles(double roubles){
        System.out.println(roubles + " рублей в доллары равно: " + roubles / rublesToDollars);
        numbersOfUsage++;
    }

    /**
     * Метод получает количество долларов для конвертации и конвертирует по установленному курсу и вывадит полученное
     * количество рублей на печать.
     * Увеличивается на 1 количество обращений к классу.
     * @param dollars количество конвертируемых долларов.
     */
    public void convertDollars(double dollars){
        System.out.println(dollars + " долларов в рубли равно: " + dollars / dollarsToRubles);
        numbersOfUsage++;
    }

    /**
     * Метод возвращаеет количество обращений к классу.
     * @return количество обращений к классу.
     */
    public static int getNumberOfUsage(){
        return numbersOfUsage;
    }
}
