package homeWork_3_1.Task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class DayOfWeek {
    byte dayNumber;

    public DayOfWeek() {
        this.dayNumber = 1;
    }

    public DayOfWeek(byte dayNumber) {
        this.dayNumber = dayNumber;
    }

    /**
     *Массив получает массив дней недели и выводит на печать указанный день недели и его название.
     * @param daysOfWeek массив дней недели.
     */
    public void getDay(String[] daysOfWeek) {
        System.out.println(dayNumber + " " + daysOfWeek[dayNumber - 1]);
    }
}
