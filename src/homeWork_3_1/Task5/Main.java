package homeWork_3_1.Task5;

public class Main {
    public static void main(String[] args) {
        String [] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        DayOfWeek dayOfWeek = new DayOfWeek();
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 2);
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 3);
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 4);
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 5);
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 6);
        dayOfWeek.getDay(daysOfWeek);
        dayOfWeek = new DayOfWeek((byte) 7);
        dayOfWeek.getDay(daysOfWeek);
    }
}
