package homeWork_3_1.Task2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //Создать первого студента
        Student student1 = new Student();
        student1.setName("Иван");
        student1.setSurname("Иванов");
        student1.setGrades(new int[]{5, 2, 3});
        //Добавить оценки
        student1.addGrades(5);
        student1.addGrades(3);
        student1.addGrades(7);
        //Создать второго студента
        Student student2 = new Student();
        student2.setName("Петр");
        student2.setSurname("Петров");
        //Добавить оценки
        student2.addGrades(4);
        student2.addGrades(3);
        student2.addGrades(7);
        //Получить данные по первому студенту
        System.out.println(student1.getName());
        System.out.println(student1.getSurname());
        System.out.println(Arrays.toString(student1.getGrades()));
        System.out.println(student1.getAverage());
        //Получить данные по второму студенту
        System.out.println(student2.getName());
        System.out.println(student2.getSurname());
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.println(student2.getAverage());
    }
}
