package homeWork_3_1.Task2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];


    public void setName(String name) {

        this.name = name;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        for (int i = 0; i < grades.length; i++) {
            this.grades[i] = grades[i];
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {

        return surname;
    }

    public int[] getGrades() {

        return grades;
    }

    /**
     * Метод добавляет новые оценки в существующий массив оценок. Если в массиве все элементы заполнены, то выполняется
     * сдвиг элементов влево на 1 и новая оценка устанавливается в конец массива.
     * @param grades переменная типа int для добавления в массив.
     */
    public void addGrades(int grades) {
        if (this.grades[9] > 0) {
            for (int i = 0; i < 9; i++) {
                this.grades[i] = this.grades[i + 1];
            }
            this.grades[9] = grades;
        } else {
            for (int i = 0; i <= 9; i++) {
                if (this.grades[i] == 0) {
                    this.grades[i] = grades;
                    break;
                }
            }
        }
    }

    /**
     * Метод суммирует все элементы массива больше 0 и делит на их количество, возвращая средний балл студента
     * @return среднее арефметическое ненулевых элементов массива.
     */
    public double getAverage() {
        double sum = 0;
        int i = 0;
        while (i <= 9 && grades[i] > 0) {
            sum += grades[i];
            i++;
        }
        return sum / i;
    }
}
