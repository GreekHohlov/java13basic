package homeWork_3_1.Task1;
/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */
public class Cat {
    /**
     * Метод выводит на печать строку Sleep
     */
    private void sleep() {
        System.out.println("Sleep");
    }
    /**
     * Метод выводит на печать строку Meow
     */
    private void meow() {
        System.out.println("Meow");
    }
    /**
     * Метод выводит на печать строку Eat
     */
    private void eat() {
        System.out.println("Eat");
    }

    /**
     * При обращении к методу генерируется случайное число от 0 до 3 и на основе полученного числа идет обращение
     * к методам, выводящим на печать действия кота.
     */
    public void status() {
        int status = (int) (Math.random() * 3);
        switch (status) {
            case 0:
                sleep();
                break;
            case 1:
                meow();
                break;
            case 2:
                eat();
                break;
        }
    }
}
