package homeWork_3_1.Task4;

public class Main {
    public static void main(String[] args) {
        //Задать время
        TimeUnit timeUnit = new TimeUnit(22, 59, 59);
        //Получить время в 24-часовом формате
        timeUnit.getTime();
        //Получить время в 12-часовом формате
        timeUnit.getTimeAmFm();
        //Добавить время
        timeUnit.setTimeUnit(5, 13, 22);
        //Получить время в 24-часовом формате
        timeUnit.getTime();
        //Получить время в 12-часовом формате
        timeUnit.getTimeAmFm();
        //Добавить время
        timeUnit.setTimeUnit(9, 13, 22);
        //Получить время в 24-часовом формате
        timeUnit.getTime();
        //Получить время в 12-часовом формате
        timeUnit.getTimeAmFm();
    }
}
