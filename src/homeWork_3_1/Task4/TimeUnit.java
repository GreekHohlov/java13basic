package homeWork_3_1.Task4;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
 */
public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeUnit() {
        hours = 0;
        minutes = 0;
        seconds = 0;
    }

    public TimeUnit(int hours) {
        if (hours < 0 || hours > 23){
            System.out.println("Введено некорректное время");
        }
        else {
            this.hours = hours;
            this.minutes = 0;
            this.seconds = 0;
        }
    }

    public TimeUnit(int hours, int minutes) {
        if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59){
            System.out.println("Введено некорректное время");
        }
        else {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = 0;
        }
    }

    public TimeUnit(int hours, int minutes, int seconds) {
        if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59){
            System.out.println("Введено некорректное время");
        }
        else {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }
    }

    /**
     * Метод выводит на печать установленное время в формате hh:mm:ss в 24-часовом формате
     */
    public void getTime(){
        System.out.printf("%02d" + ":" + "%02d" + ":" + "%02d" + "\n", hours, minutes, seconds);
    }
    /**
     * Метод выводит на печать установленное время в формате hh:mm:ss в 12-часовом формате
     */
    public void getTimeAmFm(){
        if (this.hours < 12){
            System.out.printf("%02d" + ":" + "%02d" + ":" + "%02d" + " AM" + "\n", hours, minutes, seconds);
        }
        else {
            System.out.printf("%02d" + ":" + "%02d" + ":" + "%02d" + " PM" + "\n", hours - 12, minutes, seconds);
        }
    }

    /**
     * Метод добавляет указанное время к установленному времени. Если после добавления секунд  и минут секунды и минуты
     * превышают 59, то вычитается 60 из секунд и добавляется 1 к минутам, вычетается 60 из минут и добавляется 1 к
     * часам. Если количество часов превышает 23, то из часов вычитается 24.
     * @param hours количество добавляемых часов.
     * @param minutes количество добавляемых минут.
     * @param seconds количество добавляемых секунд.
     */
    public void setTimeUnit(int hours, int minutes, int seconds){
        this.seconds += seconds;
        if (this.seconds >= 60) {
            while (this.seconds >= 60) {
                this.seconds -= 60;
                this.minutes += 1;
            }
        }
        this.minutes += minutes;
        if (this.minutes >= 60){
            while (this.minutes >= 60) {
                this.minutes -= 60;
                this.hours += 1;
            }
        }
        this.hours += hours;
        if (this.hours >= 24){
            while (this.hours >= 24) {
                this.hours -= 24;
            }
        }
    }
}
