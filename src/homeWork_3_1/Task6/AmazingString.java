package homeWork_3_1.Task6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый
 */
public class AmazingString {
    char[] array;

    /**
     * Данный конструктор передает ссылку на переданный массив массиву класса AmazingString.
     * @param array массив char.
     */
    public AmazingString(char[] array) {
        this.array = array;
    }

    /**
     * Данный конструктор преобразует полученную строку в массив char
     * @param string строка, которую необходимо преобразовать в массив.
     */
    public AmazingString(String string) {
        this.array = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            this.array[i] = string.charAt(i);
        }
    }

    /**
     * Метод возвращает элемент массива на основе полученного номерам элемента массива.
     * @param i - номер элемента массива.
     * @return эелемент массива.
     */
    public char getSymbol(int i) {
        return this.array[i];
    }

    /**
     * Метод возвращает длину массива.
     * @return длина массива.
     */
    public int getLength() {
        return this.array.length;
    }

    /**
     * В методе выводится на печать каждый элемент массива.
     */
    public void printString() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(this.array[i]);
        }
        System.out.println();
    }

    /**
     *В метод передается массив элементов char.
     *В цикле сравнивается каждый элемент исходной строки с элементом подстроки, если элемент подстроки равен элементу
     *исходной строки, то переменной типа boolean присваивается значение true. Если следующий элемент подстроки не
     *равен элементу исходной строки - цикл прерывается, переменной boolean присваивается значение false.
     * @param array массив char, наличие которого необходимо проверить в исходном массиве.
     * @return true - если подстрока содержится в строке, false - если нет.
     */
    public boolean isSubString(char[] array) {
        int k = 0;
        boolean subString = false;
        for (int i = 0; i < this.array.length; i++) {
            if (k < array.length && this.array[i] == array[k]) {
                subString = true;
                k++;
            } else if (k < array.length && this.array[i] != array[k] && subString) {
                subString = false;
                k = 0;
            } else if (k == array.length) {
                break;
            }
        }
        if (k < array.length) {
            subString = false;
        }
        return subString;
    }

    /**
     * Полученная методом строка передается в конструктор для преобразования в массив char.
     * В цикле сравнивается каждый элемент исходной строки с элементом подстроки, если элемент подстроки равен элементу
     * исходной строки, то переменной типа boolean присваивается значение true. Если следующий элемент подстроки не
     * равен элементу исходной строки - цикл прерывается, переменной boolean присваивается значение false.
     * @param string подстрока, наличие которой необходимо проверить в строке.
     * @return true - если подстрока содержится в строке, false - если нет.
     */
    public boolean isSubString(String string) {
        AmazingString amazingString = new AmazingString(string);
        int k = 0;
        boolean subString = false;
        for (int i = 0; i < this.array.length; i++) {
            if (k < amazingString.getLength() && this.array[i] == amazingString.getSymbol(k)) {
                subString = true;
                k++;
            } else if (k < amazingString.getLength() && this.array[i] != amazingString.getSymbol(k) && subString) {
                subString = false;
                k = 0;
            } else if (k == amazingString.getLength()) {
                break;
            }
        }
        if (k < amazingString.getLength()) {
            subString = false;
        }
        return subString;
    }

    /**
     * В метод передается массив char, каждый элемент сравнивается с пробелом до тех пор, если символ равен пробелу,
     * значение переменной счетчика увеличивается на 1. Если текущий символ не пробел - выполняется выход из цикла.
     * Создается временный массив размером, равным исходному массиву минус количество ведущих пробелов, ему
     * присваиваются значения из исходного массива, начиная с переменной счетчика. После заполнения ссылка на массив
     * передается исходному массиву.
     */
    public void cutSpace() {
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == ' ') {
                k++;
            } else {
                break;
            }
        }
        char[] tempArray = new char[array.length - k];
        for (int i = 0; i < tempArray.length; i++) {
            tempArray[i] = array[i + k];
        }
        array = tempArray;
    }

    /**
     *В метод передается массив char и в цикле разворачивает строку с использоваением временной переменной.
     */
    public void reversArray() {
        char temp;
        for (int i = 0; i < array.length / 2; i++) {
            temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    }
}
