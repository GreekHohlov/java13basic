package homeWork_3_1.Task6;

public class Main {
    public static void main(String[] args) {
        //Работа с массивом
        char[] test = {' ', ' ', ' ', 'a', 'b', 'a', 'c', 'd', 'e'};
        char[] testSubString = {'d', 'e'};
        AmazingString amazingString = new AmazingString(test);
        System.out.println(amazingString.getSymbol(2));
        amazingString.printString();
        amazingString.cutSpace();
        amazingString.printString();
        System.out.println(amazingString.getLength());
        System.out.println(amazingString.isSubString(testSubString));
        amazingString.printString();
        amazingString.reversArray();
        amazingString.printString();

        //Работа с строкой
        AmazingString amazingString1 = new AmazingString("тестировщик");
        amazingString1.printString();
        amazingString1.cutSpace();
        amazingString1.printString();
        String test2 = "овщик";
        System.out.println(amazingString1.getSymbol(3));
        System.out.println(amazingString1.isSubString(test2));
        System.out.println(amazingString1.getLength());
        amazingString1.printString();
        amazingString1.reversArray();
        amazingString1.printString();

    }
}
