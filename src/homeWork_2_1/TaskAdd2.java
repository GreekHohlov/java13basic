package homeWork_2_1;

import java.util.Scanner;

public class TaskAdd2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); //Получение размера одномерного массива
        int[] array = new int[n]; //Изначальный массив
        int[] arraySort = new int[n]; //Массив, отсортированный по возрастанию после возведения
                                      //элементов изначального массива в степень 2
        /*
        В следующем цикле выполняется сортировка изначального массива.
        Сравниваются по модулю начальный и конечный элемент массива, больший элемент возводится в квадрат
        и записывается в массив arraySort с конца массива, дальше выполняется сравнение оставшихся символов.
         */
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0,  j = 0; i <= array.length - 1; i++) {
            if (array[0] >= 0){
                arraySort[i] = array[i] * array[i];
            }
            else if (Math.abs(array[i - j]) > Math.abs(array[array.length - 1 - j])){
                arraySort[array.length - 1 - i] = array[i - j] * array[i - j];
            }else {
                arraySort[arraySort.length - 1 - i] = array[array.length - 1 - j] * array[array.length - 1 - j];
                j++;
            }
        }
        System.out.print(arraySort[0]); //Вывод нового массива в консоль.
        for (int i = 1; i < n; i++) {
            System.out.print(" " + arraySort[i]);
        }
    }
}
