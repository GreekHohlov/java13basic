package homeWork_2_1;


import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        for (int i = 0; i < m; i++) {
            int first = array[array.length - 1];
            System.arraycopy(array, 0, array, 1, array.length - 1);
            array[0] = first;
        }
        System.out.print(array[0]);
        for (int i = 1; i < n; i++) {
            System.out.print(" " + array[i]);
        }
    }
}
