package homeWork_2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        array = arraySort(array);

        if (indexSearch(m, array) < -n){
            System.out.println(array[n - 1]);
        }
        else {
            if (indexSearch(m, array) >= 0) {
                System.out.println(array[indexSearch(m, array)]);
            } else {
                int low = -indexSearch(m, array) - 1;
                int high = -indexSearch(m, array);
                if (m - low == high - m) {
                    System.out.println(array[high]);
                } else if (m - low > high - m) {
                    System.out.println(array[high]);
                } else {
                    System.out.println(array[low]);
                }
            }
        }
    }
    public static int[] arraySort(int[] arrayForSort){
        Arrays.sort(arrayForSort);
        return arrayForSort;
    }
    public static int indexSearch(int key, int[] arrayForSearch){
        int low = 0;
        int high = arrayForSearch.length-1;
        while (high >= low){
            int mid = (low + high) / 2;
            if (key < arrayForSearch[mid]){
                high = mid - 1;
            }
            else if (key == arrayForSearch[mid]) {
                return mid;
            }
            else {
                low = mid + 1;
            }
        }
        return -low - 1;
    }
}
