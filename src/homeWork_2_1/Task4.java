package homeWork_2_1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int number = array[0];
        int repeat = 1;
        for (int i = 1; i < n; i++) {
            if (array[i] == number && i == array.length - 1){
                System.out.println((++repeat) + " " + array[i]);
                break;
            }
            if (array[i] == number){
                repeat++;
            }
            if (array[i] > number && i == array.length - 1){
                System.out.println(repeat + " " + array[i - 1]);
                System.out.println(1 + " " + array[i]);
                break;
            }
            if (array[i] > number){
                System.out.println(repeat + " " + array[i - 1]);
                number = array[i];
                repeat = 1;
            }
        }
    }
}
