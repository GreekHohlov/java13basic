package homeWork_2_1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        convertString(s);
    }

    public static void convertString(String s){
        String[] morse = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--",
                "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--",
                "-.--", "-..-", "..-..", "..--", ".-.-"};
        String sToMorse = "";
        for (int i = 0; i < s.length(); i++) {
            sToMorse += morse[(s.charAt(i) - 1040)] + " ";
        }
        System.out.println(sToMorse.trim());
    }
}
