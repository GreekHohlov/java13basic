package homeWork_2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        array = arrayExponentiation(array);
        arraySort(array);
    }

    public static int[] arrayExponentiation(int[] arrayExponent){
        for (int i = 0; i < arrayExponent.length; i++){
            arrayExponent[i] *= arrayExponent[i];
        }
        return arrayExponent;
    }

    public static void arraySort(int[] arraySort){
        Arrays.sort(arraySort);
        System.out.print(arraySort[0]);
        for (int i = 1; i < arraySort.length; i++) {
            System.out.print(" " + arraySort[i]);
        }
    }
}
