package homeWork_2_1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            if (x < array[i]) {
                System.out.println(i);
                break;
            }
            if (x >= array[i] && i == array.length - 1){
                System.out.println(i + 1);
            }
        }
    }
}
