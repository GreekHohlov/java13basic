package homeWork_2_1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextByte();
        int[] array1 = new int[n];
        for (int i = 0; i < n; i++) {
            array1[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] array2 = new int[m];
        for (int i = 0; i < m; i++) {
            array2[i] = scanner.nextInt();
        }
        boolean flag = true;
        if (n == m){
            for (int i = 0; i < n; i++) {
                if (array1[i] != array2[i]){
                    flag = false;
                    break;
                }
            }
        }
        else{
            flag = false;
        }
        System.out.println(flag);
    }
}
