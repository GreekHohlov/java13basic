package homeWork_2_1;

import java.util.Random;
import java.util.Scanner;

public class TaskAdd1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n; //Объявление переменной для задания длины пароля.
        do { //Цикл, выполняющий проверку длины пароль, пароль должен быть не менее 8 символов
            n = scanner.nextInt();
            if (n < 8) {
                System.out.print("Пароль с " + n + " количеством символов небезопасен" +
                        "\nВведите пароль еще раз: ");
            }
        } while (n < 8);
        passwordGenerate(n); //Вызов метода генерации случайного пароля.
    }

    /**
     * Данный метод генерирует случайный пароль. Длина пароля задается пользователем в методе main и передается в
     * метод.
     * Генерируемый пароль содержит следующий набор символов: a-z, A-Z, 0-9, *, -, _
     * Создается массив длины n.
     * В первом цикле заполняется массив с 0 по 5 элемент символами, обязательными к содержанию в пароле.
     * В следующем цикле выполняется дозаполнение массива случайными символами, после чего выполняется перетасовка
     * массива и вывод в консоль
     *
     * @param n - Длина генерируемого пароля.
     */
    public static void passwordGenerate(int n) {
        Random random = new Random();
        String[] array = new String[n];


        for (int i = 0; i < 4; i++) {//Заполнение массива с 0 по 5 элемент обязательными символами.

            switch (i) {
                case (0):
                    array[i] = getUpCase();
                    break;
                case (1):
                    array[i] = getLowCase();
                    break;
                case (2):
                    array[i] = getNumberCase();
                    break;
                case (3):
                    array[i] = getSymbolCase();
                    break;
            }
        }
        for (int i = 4; i < n; i++) { //Заполнение оствашейся части массива случайными символами
            int caseNumber = random.nextInt(4);
            switch (caseNumber) {
                case (0):
                    array[i] = getUpCase();
                    break;
                case (1):
                    array[i] = getLowCase();
                    break;
                case (2):
                    array[i] = getNumberCase();
                    break;
                case (3):
                    array[i] = getSymbolCase();
                    break;
            }
        }

        for (int i = 0; i < n; i++) { //Перетасовка элеметов массива
            int numberToSwitch = random.nextInt(6);
            String temp = array[i];
            array[i] = array[numberToSwitch];
            array[numberToSwitch] = temp;
        }

        for (int i = 0; i < n; i++) {//Выпод пароля
            System.out.print(array[i]);
        }
    }

    /**
     * Метод генерирует случайный символ от A-Z
     *
     * @return случайный символ от A-Z
     */
    public static String getUpCase() {
        return String.valueOf((char) ((int) (Math.random() * 26 + 65)));
    }

    /**
     * Метод генерирует случайный символ от a-z.
     *
     * @return случайный символ от a-z.
     */
    public static String getLowCase() {
        return String.valueOf((char) ((int) (Math.random() * 26 + 97)));
    }

    /**
     * Метод генерирует случайный символ от 0-9.
     *
     * @return случайный символ от 0-9.
     */
    public static String getNumberCase() {
        return String.valueOf((char) ((int) (Math.random() * 10 + 48)));
    }

    /**
     * Метод генеририрует случайное число от 0 до 3 (индекс элемента массива {"-", "_", "*"}) и возвращает элемент
     * массива с этим индексом.
     *
     * @return один из символов - _ *
     */
    public static String getSymbolCase() {
        String[] symbol = {"-", "_", "*"};
        return symbol[(int) (Math.random() * 3)];
    }
}
