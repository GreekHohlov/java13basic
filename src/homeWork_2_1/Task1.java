package homeWork_2_1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] array = new double[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextDouble();
        }
        averageArray(array);

    }
    static void averageArray(double[] array){
        double average = 0;
        for (int i = 0; i < array.length; i++) {
            average += array[i];
        }
        System.out.println(average / (array.length));
    }

}
