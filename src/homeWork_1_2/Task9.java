package homeWork_1_2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        if (x > - 1000 && x < 1000) {
            if ((int)(Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2) - 1) == 0){
                System.out.println("true");
            }
            else {
                System.out.println("false");
            }
        }
    }
}
