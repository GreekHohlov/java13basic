package homeWork_1_2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        if (count > 0 && count < 10000){
            if (count < 500){
                System.out.println("beginner");
            }
            if (count >= 500 && count < 1500){
                System.out.println("pre-intermediate");
            }
            if (count >= 1500 && count < 2500){
                System.out.println("intermediate");
            }
            if (count >= 2500 && count < 3500){
                System.out.println("upper-intermediate");
            }
            if (count >= 3500){
                System.out.println("fluent");
            }
        }
    }
}
