package homeWork_1_2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x;

        System.out.print("Введите порядковый день недели: ");
        x = input.nextInt();

        if (x >=1 && x <= 7){
            if (x <=5){
                System.out.println(6 - x);
            }
            else {
                System.out.println("Ура, выходные!");
            }
        }
        else {
            System.out.println("Введены некорректные данные");
        }
    }
}
