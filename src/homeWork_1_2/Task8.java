package homeWork_1_2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        if (s.length() > 2 && s.length() < 100) {
            if (s.indexOf(' ', s.indexOf(' ') + 1) != -1){
                System.out.println(s.substring(0, s.indexOf(' ', s.indexOf(' ') + 1)) + "\n" +
                        s.substring(s.indexOf(' ', s.indexOf(' ') + 1) + 1));
            }
            else {
                System.out.println(s);
            }
        }
    }
}
