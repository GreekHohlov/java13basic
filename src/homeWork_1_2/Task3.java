package homeWork_1_2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x;

        System.out.print("Введите текущее время: ");
        x = input.nextInt();

        if (x >= 0 && x <= 23){
            if (x > 12){
                System.out.println("Пора");
            }
            else{
                System.out.println("Рано");
            }
        }
        else{
            System.out.println("Введены некорректные данные");
        }
    }
}
