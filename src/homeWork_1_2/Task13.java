package homeWork_1_2;

import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String model = input.nextLine();
        int price = input.nextInt();
        if (model.contains("iphone") || model.contains("samsung") && price >= 50000 && price <= 120000){
            System.out.println("Можно купить");
        }
        else {
            System.out.println("Не подходит");
        }
    }
}
