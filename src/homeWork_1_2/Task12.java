package homeWork_1_2;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String mailPackage = input.nextLine();
        if (mailPackage.contains("камни!") && mailPackage.contains("запрещенная продукция")) {
            System.out.println("в посылке камни и запрещенная продукция");
        }
        else if (mailPackage.contains("камни!")) {
            System.out.println("камни в посылке");
        }
        else if (mailPackage.contains("запрещенная продукция")){
            System.out.println("в посылке запрещенная продукция");
        }
        else {
            System.out.println("все ок");
        }
    }
}
