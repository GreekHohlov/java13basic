package homeWork_1_2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        if (s.length() > 2 && s.length() < 100) {
            System.out.println(s.substring(0, s.indexOf(' ')) + "\n" + s.substring(s.indexOf(' ') + 1));
        }
    }
}
