package homeWork_1_2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        if (a > -100 && a < 100 && b > -100 && b < 100 && c > -100 && c < 100){
            if (Math.pow(b, 2) - 4 * a * c >= 0){
                System.out.println("Решение есть");
            }
            else {
                System.out.println("Решения нет");
            }
        }
    }
}
