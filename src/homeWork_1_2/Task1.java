package homeWork_1_2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a, b, c; //Оценки за год работы Пети
        System.out.print("Введите оценки работы Пети за последние 3 года: ");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        if (a > 0 && a < 100 && b > 0 && b < 100 && c > 0 && c < 100 ) {
            if (a > b && b > c) {
                System.out.println("Петя, пора трудиться!");
            }
            else {
                System.out.println("Петя молодец!");
            }
        }
        else{
            System.out.println("Введены некорректные данные.");
        }
    }
}
