package homeWork_1_2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        if (n > - 500 && n < 500) {
            if ( Math.log(Math.pow(Math.E, n)) == n){
                System.out.println("true");
            }
            else {
                System.out.println("false");
            }
        }
    }
}
