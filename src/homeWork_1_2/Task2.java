package homeWork_1_2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x, y; //Координаты точки

        System.out.print("Введите координаты точки: ");
        x = input.nextInt();
        y = input.nextInt();

        if (x > -100 && x < 100 && y > -100 && y < 100){
            System.out.println(x > 0 && y > 0);
        }
        else {
            System.out.println("Введены некорректные данные");
        }
    }
}
