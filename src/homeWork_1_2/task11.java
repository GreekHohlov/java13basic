package homeWork_1_2;

import java.util.Scanner;

public class task11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        boolean upCase = false;
        boolean lowCase = false;
        boolean specSym = false;
        boolean numb = false;
        if (s.length() >= 8) {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z') {
                    lowCase = true;
                }
                if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
                    upCase = true;
                }
                if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
                    numb = true;
                }
                if (s.charAt(i) == '_' || s.charAt(i) == '-' || s.charAt(i) == '*') {
                    specSym = true;
                }
            }
        }
        if (lowCase && upCase && numb && specSym) {
            System.out.println("пароль надежный");
        }
        else{
            System.out.println("пароль не прошел проверку");
        }

    }
}
