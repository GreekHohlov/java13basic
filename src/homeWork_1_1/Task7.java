package homeWork_1_1;

import java.util.Scanner;

/*
    На вход подается двузначное число n. Выведите число, полученное перестановкой цифр в исходном числе n.
    Если после перестановки получается ведущий 0, его также надо вывести.

    Ограничения:
    9 < count < 100

    Входные данные:             Выходные данные:
    1. 45                       1. 54
    2. 10                       2. 01
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите двузначное число: ");
        int count = scanner.nextInt();

        if (count >9 && count < 100){
            int a = count / 10;
            int b = count % 10;
            System.out.println(b + "" + a);
        }else{
            System.out.println("Введены некорректные значения");
        }
    }
}
