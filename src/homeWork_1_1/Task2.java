package homeWork_1_1;

import java.util.Scanner;
/*
    На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.
    Подсказка:
    Среднее квадратическое: https://en.wikipedia.org/wiki/Root_mean_square
    Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)

    Ограничения:
    0 < a, b < 100

    Входные данные:             Выходные данные:
    1. a = 35; b = 5;           1. 25.0
    2. a = 23; b = 70;          2. 52.100863716449076
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a, b;
        double rms;
        System.out.print("Введите два целых числа:");
        a = scanner.nextInt();
        b = scanner.nextInt();

        if (a > 0 && b < 100){
            rms = Math.sqrt(1.0 / 2 * (Math.pow(a , 2) + Math.pow(b , 2)));
            System.out.println("Среднее квадратичное равно: " + rms);
        }else{
            System.out.println("Введены некорректные данные");
        }
    }
}
