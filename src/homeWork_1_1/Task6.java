package homeWork_1_1;

import java.util.Scanner;

/*
    На вход подается количество километров count. Переведите километры в мили
    (1 миля = 1,60934 км) и выведите количество миль.

    Ограничения:
    0 < count < 1000

    Входные данные:         Выходные данные:
    1. 7                    1. 4.349609156548647
    2. 143                  2. 88.85630134092237
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double mile;

        System.out.print("Введите количество километров от 1 до 1000: ");
        int count = scanner.nextInt();

        if (count > 0 && count < 1000){
            mile = count / 1.60934;
            System.out.println(count + " километров равно " + mile + " миль");
        }else{
            System.out.println("Введено некорректное значение");
        }

    }
}
