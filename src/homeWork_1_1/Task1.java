package homeWork_1_1;

import java.util.Scanner;

/*
    Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
    Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi взять из
    Math.

    Ограничения:
    0 < r < 100

    Входныые данные:        Выходные данные:
    1. 9                    1. 3053.6280592892786
    2. 25                   2. 65449.84694978735
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double r; //Радиус шара
        double v; //Объём шара

        System.out.print("Введите радиус шара в диапазоне от 0 до 100: ");
        r = scanner.nextDouble();

        if (r > 0 && r < 100){
            v = 4.0 / 3 * Math.PI * Math.pow(r, 3);
            System.out.println("Объём шара равен: " + v);
        }else{
            System.out.println("Введены некорректное значение");
        }
    }
}
