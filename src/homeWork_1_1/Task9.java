package homeWork_1_1;

import java.util.Scanner;

/*
    На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k тугриков.
    Вычислите и выведите, сколько гостей можно пригласить на мероприятие.

    Ограничения:
    0 < n < 100000
    0 < k < 1000
    k < n

    Входные данные:                 Выходные данные:
    1. n = 14185, k= 72;            1. 197
    2. n = 85177, k = 89;           2. 957
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int peoples;

        System.out.print("Введите бюждет мероприятия и бюждет на одного гостя: ");
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        if ((n > 0 && n < 100000) && (k > 0 && k < 1000) && k < n){
            peoples = n / k;
            System.out.println("Вы можете пригласить " + peoples + " гостей.");
        }else{
            System.out.println("Введены некорректные данные");
        }
    }
}
