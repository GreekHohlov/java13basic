package homeWork_1_1;

import java.util.Scanner;

/*
    На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30 дней.

    Ограничения:
    0 < count < 100000

    Входные данные:     Выходные данные:
    1. 13509            1. 450.3
    2. 81529            2. 2717.633333333333
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double daylyBudget;
        System.out.print("Введите баланс счёта: ");
        int n = scanner.nextInt();

        if (n > 0 && n < 100000){
            daylyBudget = n / 30.0;
            System.out.println("Дневной бюджет равен: " + daylyBudget);
        }else{
            System.out.println("Введено некорректное значение");
        }
    }
}
