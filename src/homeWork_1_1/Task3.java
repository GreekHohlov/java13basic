package homeWork_1_1;

import java.util.Scanner;

/*
    Прочитайте из консоли имя пользователя и выведите в консоль строку: Привет, <имя пользователя>!

    Подсказка:
    Получите данные из консоли c помощью объекта Scanner, сохраните в переменную userName и выведите в консоль
    с помощью System.out.println()

    Ограничения:
    0 < длина имени пользователя < 100

    Входные данные:         Выходные данные:
    Иван                    Привет, Иван!
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String userName;
        int l; //Переменная для хранения длины имени пользователя

        System.out.print("Введите имя пользователя: ");
        userName = scanner.nextLine();
        l = userName.length();

        if (l > 0 && l < 100){
            System.out.println("Привет, " + userName + "!");
        }else{
            System.out.println("Некорректное колличество символов в имени пользователя!");
        }
    }
}
