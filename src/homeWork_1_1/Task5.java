package homeWork_1_1;

import java.util.Scanner;

/*
    Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход подается количество дюймов,
    выведите количество сантиметров.

    Ограничения:
    0 < count < 1000

    Входные данные:         Выходные данные:
    1. 12                   1. 30.48
    2. 99                   2. 251.46
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double santim;
        System.out.print("Введите количество дюймов: ");
        int inch = scanner.nextInt();

        if (inch > 0 && inch < 1000){
            santim = inch * 2.54;
            System.out.println(inch + " дюймов равно " + santim + " сантиметров");
        }else{
            System.out.println("Введено некорректное количество дюймов");
        }

    }
}
