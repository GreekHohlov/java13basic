package homeWork_1_1;

import java.util.Scanner;

/*
    На вход подается количество секунд, прошедших с начала текущего дня – count.
    Выведите в консоль текущее время в формате: часы и минуты.

    Ограничения:
    0 < count < 86400
    Входные данные:         Выходные данные:
    1. 32433                1. 9 0
    2. 41812                2. 11 36
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int hours, minutes;
        System.out.print("Введите количество секунд от 1 до 86400: ");
        int sec = scanner.nextInt();

        if(sec > 0 && sec < 86400){
            hours = sec / 3600;
            minutes = sec / 60 % 60;
            System.out.println("Текущее время " + hours + ":" + minutes);
        }else{
            System.out.println("Введено некорректное колличество секунд");
        }
    }
}
