package ProfessionalModule.HW4.task4;
import java.util.List;
/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(15.3, 17.1, 6.1, 10.4, 6.2, 4.9, 1.5, 15.2);
        list.stream()
                .sorted(((o1, o2) -> o1.compareTo(o2)))
                .forEach(System.out::println);
    }
}
