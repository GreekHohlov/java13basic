package ProfessionalModule.HW4.task1;

import java.util.ArrayList;
import java.util.List;
/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        int i = 0;
        while (i <= 100){
            list.add(i);
            i++;
        }
        Integer sum = list.stream()
                .filter(number -> number % 2 == 0)
                .reduce(0, Integer::sum);

        System.out.println(sum);
    }
}
