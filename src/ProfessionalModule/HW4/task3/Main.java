package ProfessionalModule.HW4.task3;
import java.util.List;
/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");

        List<String> newList = list.stream()
                .filter(str -> !str.isEmpty()).toList();
        System.out.println(newList.size());
    }
}
