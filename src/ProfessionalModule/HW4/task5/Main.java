package ProfessionalModule.HW4.task5;
/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        AtomicInteger i = new AtomicInteger(list.size());
        list.stream()
                .map(s1 -> s1.toUpperCase())
                .forEach(element -> {
                    int j = i.getAndDecrement() - 1;
                    if (j > 0){
                        System.out.print(element + ", ");
                    } else {
                        System.out.print(element + ".");
                    }
                });
    }
}
