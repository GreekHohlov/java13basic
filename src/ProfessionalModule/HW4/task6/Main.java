package ProfessionalModule.HW4.task6;
/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
*/
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        set1.add(1);
        set1.add(3);
        set1.add(16);
        set1.add(2);

        set2.add(45);
        set2.add(25);
        set2.add(11);

        Set<Set<Integer>> set = new HashSet<>();
        set.add(set1);
        set.add(set2);

        Set<Integer> newSet = set.stream()
                .flatMap(set3 -> set3.stream())
                .collect(Collectors.toSet());
        System.out.println(newSet);
    }
}
