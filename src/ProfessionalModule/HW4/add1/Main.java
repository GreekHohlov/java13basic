package ProfessionalModule.HW4.add1;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */
public class Main {
    public static void main(String[] args) {
        String s1 = "cat";
        String s2 = "cats";
        String s3 = "cat";
        String s4 = "cut";
        String s5 = "cat";
        String s6 = "nut";
        String s7 = "cat";
        String s8 = "cuts";

        checkString(s1, s2);
        checkString(s3, s4);
        checkString(s5, s6);
        checkString(s7, s8);
    }

    public static void checkString(String s1, String s2) {
        int k = 0;
        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) == s2.charAt(i)) {
                    k++;
                }
            }
            System.out.println(s1.length() - k <= 1);
        } else if (Math.abs(s1.length() - s2.length()) == 1) {
            if (s1.length() > s2.length()) {
                System.out.println(s1.contains(s2));
            }
            if (s1.length() < s2.length()) {
                System.out.println(s2.contains(s1));
            }
        } else {
            System.out.println(false);
        }
    }
}
