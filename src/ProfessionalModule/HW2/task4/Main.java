package ProfessionalModule.HW2.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();
        Document document1 = new Document(1, "Java. Полное руководство", 1500);
        Document document2 = new Document(2, "SQL. Полное руководство. Третье издание", 958);
        Document document3 = new Document(3, "С# 4.0. Полное руководство", 1056);
        Document document4 = new Document(4, "Python Essential Reference", 858);
        Document document5 = new Document(5, "Android NDK для начинающих", 518);
        Organizer organizer = new Organizer();

        list.add(document1);
        list.add(document2);
        list.add(document3);
        list.add(document4);
        list.add(document5);

        System.out.println(organizer.organizeDocuments(list));
    }
}
