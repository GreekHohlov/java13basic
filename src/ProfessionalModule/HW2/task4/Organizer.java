package ProfessionalModule.HW2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Organizer {
    public Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> map = new HashMap<>();
        for (Document element: documents) {
            map.put(element.id, element);
        }
        return map;
    }
}
