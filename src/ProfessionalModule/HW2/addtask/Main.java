package ProfessionalModule.HW2.addtask;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};
        int k = 4;
        System.out.println(getWords(words, k));
    }

    /**
     * В метоже формируется Map<String, Integer> из полученного набора слов и их повторений.
     * Создается список, который вызвает метод сортировки getSortMap из класса Sort, передавая в него
     * сформированный Map и количество необходимых слов и получет из этого метода отсортированный список слов по
     * количеству повторений и, если есть слова с одинаковым количеством повторений, то эти слова отсортированы в
     * в лексикографическом порядке.
     *
     * @param words массив слов для сортировки
     * @param k     количество возвращаемых слов
     * @return список отсортированных слов
     */
    public static Set<String> getWords(String[] words, int k) {
        Map<String, Integer> list = new LinkedHashMap<>();
        for (String value : words) {
            int number = 1;
            if (list.containsKey(value)) {
                number = list.get(value);
                number++;
            }
            list.put(value, number);
        }
        Set<String> set = new LinkedHashSet<>();
        set = Sort.getSortMap(list, k);
        return set;
    }
}
