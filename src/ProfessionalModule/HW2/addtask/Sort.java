package ProfessionalModule.HW2.addtask;

import java.util.*;

public class Sort {
    /**
     * Метод принимает Map<String, Integer> и значение, указываеющее на количество возвращаемых слов
     * из метода public static getWords класса Main для сортировки Map<String, Integer> по полю V.
     *
     * @param list Map<String, Integer>
     * @param k    кооличество возвращаемых сло
     * @return список строк
     */
    public static Set<String> getSortMap(Map<String, Integer> list, int k) {
        Set<String> set = new LinkedHashSet<>();
        //Создаются два массива для хранения полученных слов и количества их повторов.
        String[] stringArray = new String[list.size()];
        int[] intArray = new int[list.size()];
        int i = 0;
        //Заполнение массивов словами и их количеством.
        for (String element : list.keySet()) {
            stringArray[i] = element;
            intArray[i] = list.get(element);
            i++;
        }
        //Сортировка массивов по значению в массиве intArray[]
        for (int j = 0; j < intArray.length; j++) {
            for (int l = 1; l < intArray.length; l++) {
                if (intArray[l - 1] > intArray[l]) {
                    int tempInt = intArray[l - 1];
                    intArray[l - 1] = intArray[l];
                    intArray[l] = tempInt;
                    String tempString = stringArray[l - 1];
                    stringArray[l - 1] = stringArray[l];
                    stringArray[l] = tempString;
                }
            }
        }
        //Создание временной Map, в которую добавляются слова и их количество, если слова повторяются
        //одинаковое количество раз для дальнейшей сортировки таких слов в лексикографическом порядке.
        //TreeMap автоматически сортирует плученный набор данных, поэтому в данном случае выбрана эта реализация Map,
        //в остальных случаях выбрана реализация LinkedHashMap и LinkedHashSet, так как они сохраняют элементы в
        //порядке добавления.
        Map<String, Integer> tempMap = new TreeMap<>();
        for (int j = 0; j < intArray.length - 1; j++) {
            for (int l = 1 + j; l < intArray.length; l++) {
                if (intArray[l] == intArray[j]) {
                    tempMap.put(stringArray[j], intArray[j]);
                    tempMap.put(stringArray[l], intArray[l]);
                }
            }
        }
        //Очистка исходной Map и её последующее заполнение из массивов intArray,stringArray и из tempMap.
        //Проверяется первое значние в повторяющихся словах и сравнивается с intArray[i], большее значение записывается
        //в Map list. Значение i изначально равно количеству слов, если к концу добавления всех слов из tempArray
        //значение i не отрицательное, то list продолжает заполняться данными из intArray и stringArray, пока не
        //добавятся все значения.
        list.clear();
        i = intArray.length - 1;
        for (String element : tempMap.keySet()) {
            if (tempMap.get(element) < intArray[i]) {
                while (tempMap.get(element) < intArray[i]) {
                    list.put(stringArray[i], intArray[i]);
                    i--;
                }
                list.put(element, tempMap.get(element));
            } else {
                list.put(element, tempMap.get(element));
                i--;
            }
        }
        while (i >= 0) {
            list.put(stringArray[i], intArray[i]);
            i--;
        }
        //Формируется список из колличества слов, указанных в значении k и возвращается вызвавшему данный метод методу
        //getWords из класса Main.
        for (String element : list.keySet()) {
            if (k > 0) {
                set.add(element);
                k--;
            } else {
                break;
            }
        }
        return set;
    }
}
