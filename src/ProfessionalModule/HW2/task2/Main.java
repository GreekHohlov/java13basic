package ProfessionalModule.HW2.task2;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первую строку: ");
        String s = scanner.nextLine();
        System.out.print("Введите вторую строку: ");
        String t = scanner.nextLine();
        checkAnagram(s, t);
    }

    public static void checkAnagram(String s, String t) {
        Set<Character> set1 = new TreeSet<>();
        Set<Character> set2 = new TreeSet<>();
        for (char ch : s.toCharArray()) {
            set1.add(ch);
        }
        for (char ch : t.toCharArray()) {
            set2.add(ch);
        }
        System.out.println(set1.containsAll(set2) || set2.containsAll(set1));
    }
}
