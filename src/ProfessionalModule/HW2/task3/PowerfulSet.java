package ProfessionalModule.HW2.task3;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class PowerfulSet {
    public PowerfulSet() {

    }

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T element1 : set1) {
            for (T element2 : set2) {
                if (element1 == element2) {
                    result.add(element1);
                }
            }
        }
        return result;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> result = new TreeSet<>();
        result.addAll(set1);
        result.addAll(set2);
        return result;
    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T element1 : set1) {
            if (!set2.contains(element1)) {
                result.add(element1);
            }
        }
        return result;
    }
}
