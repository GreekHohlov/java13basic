package ProfessionalModule.HW2.task1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Sifter {

    public static <T> Set<T> getUniqueElement(ArrayList<T> array){
        Set<T> sifterArray = new LinkedHashSet<>();
        for (T element: array) {
            if (!sifterArray.contains(element)){
                sifterArray.add(element);
            }
        }
        return sifterArray;
    }
}
