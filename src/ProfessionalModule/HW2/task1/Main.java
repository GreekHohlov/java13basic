package ProfessionalModule.HW2.task1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> array = new ArrayList<>();
        array.add("one");
        array.add("hot");
        array.add("word");
        array.add("had");
        array.add("one");
        array.add("the");
        array.add("one");
        array.add("had");
        array.add("other");
        array.add("word");

        System.out.println(Sifter.getUniqueElement(array));
    }
}
