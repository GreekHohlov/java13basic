package ProfessionalModule.HW1.add1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        getElements(array);
    }

    public static void getElements(int[] array) {
        java.util.Arrays.sort(array);
        for (int i = 0; i < 2; i++) {
            System.out.print(array[array.length - 1 - i] + " ");
        }
    }
}
