package ProfessionalModule.HW1.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    private static final String FILE_PATH = "C:\\Users\\bit1.000\\YandexDisk\\Studies\\homeWork\\src\\ProfessionalModule\\HW1\\task1";

    public static void main(String[] args) throws MyCheckException {
        readFile();
    }

    public static void readFile() throws MyCheckException {
        try (Scanner scanner = new Scanner(new File(FILE_PATH + "\\input.txt"));) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
            }
        } catch (FileNotFoundException e) {
            throw new MyCheckException();
        }
    }
}
