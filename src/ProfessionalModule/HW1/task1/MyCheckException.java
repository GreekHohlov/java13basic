package ProfessionalModule.HW1.task1;

import java.io.FileNotFoundException;

public class MyCheckException extends FileNotFoundException {
    public MyCheckException() {
        super("Файл не найден!");
    }
}
