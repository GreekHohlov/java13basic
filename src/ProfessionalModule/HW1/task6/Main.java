package ProfessionalModule.HW1.task6;

public class Main {
    public static void main(String[] args) throws Exception {
        FormValidator.checkName("Petya");
        FormValidator.checkName("Петя");
        FormValidator.checkGender("Male");
        FormValidator.checkBirthdate("1987.05.06");
        FormValidator.checkHeight("175.5");
    }
}
