package ProfessionalModule.HW1.task6;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class FormValidator {
    private FormValidator() {

    }

    public static void checkName(String str) throws Exception {
        if (str.matches("[A-Z][a-z]{1,19}") || str.matches("[А-Я][а-я]{1,19}")) {
            System.out.println("Валидация имени пройдена");
        } else {
            throw new Exception("Неверный формат имени");
        }
    }

    public static void checkBirthdate(String str) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy.MM.dd");
        Date startDate = format.parse("1900.01.01");
        Date endDate = new Date();

        Date date = format.parse(str);
        if (date.after(startDate) && date.before(endDate)) {
            System.out.println("Валидация даты пройдена");
        } else {
            throw new Exception("Неорректная дата");
        }
    }

    public static void checkGender(String str) throws Exception {
        if (Gender.MALE.name.equalsIgnoreCase(str) || Gender.FEMALE.name.equalsIgnoreCase(str)) {
            System.out.println("Валидация пола пройдена");
        } else {
            throw new Exception("Введен некорректный пол");
        }
    }

    public static void checkHeight(String str) throws Exception {
        try {
            double hight = Double.parseDouble(str);
            if (hight > 0) {
                System.out.println("Валидация роста пройдена");
            } else {
                throw new Exception("Рост должен быть положительным числом");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
