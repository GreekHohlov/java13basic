package ProfessionalModule.HW1.task2;

public class Main {
    public static void main(String[] args) {
        int[] array = {5, 4, 4, 5, 8, 9};
        outputArray(array, 2);
        outputArray(array, 5);
        outputArray(array, 6);
    }

    public static void outputArray(int[] array, int i) {
        try {
            System.out.println(array[i]);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MyUncheckException();
        }
    }
}
