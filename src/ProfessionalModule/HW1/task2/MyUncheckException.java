package ProfessionalModule.HW1.task2;

public class MyUncheckException extends ArrayIndexOutOfBoundsException {
    public MyUncheckException() {
        super("Выход за пределы массива");
    }

    public MyUncheckException(String message) {
        super(message);
    }
}
