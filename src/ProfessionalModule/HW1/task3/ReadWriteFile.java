package ProfessionalModule.HW1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class ReadWriteFile {
    private static final String FILE_PATH = "C:\\Users\\bit1.000\\YandexDisk\\Studies\\homeWork\\src\\ProfessionalModule\\HW1\\task3\\";

    public static void readWriteFile() throws IOException {

        try (Scanner scanner = new Scanner(new File(FILE_PATH + "\\input.txt"));
             Writer writer = new FileWriter(new File(FILE_PATH + "\\output.txt"));) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().toUpperCase() + "\n";
                writer.write(line);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
