package ProfessionalModule.HW1.task4;

public class Main {
    public static void main(String[] args) throws Exception {
        setNumber(2);
        setNumber(3);
    }

    public static void setNumber(int n) throws Exception {
        if (n % 2 == 0) {
            new MyEvenNumber(n);
        } else {
            throw new Exception();
        }
    }
}
