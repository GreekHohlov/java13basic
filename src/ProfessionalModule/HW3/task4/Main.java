package ProfessionalModule.HW3.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        getInterfaces(F.class);
    }

    public static void getInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        for (Class<?> clazz : interfaces) {
            System.out.println(clazz.getName());
        }
    }
}
