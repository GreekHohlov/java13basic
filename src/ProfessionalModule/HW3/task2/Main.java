package ProfessionalModule.HW3.task2;

import ProfessionalModule.HW3.task1.IsLike;
import ProfessionalModule.HW3.task1.MyIsLakeClass;

public class Main {
    public static void main(String[] args) {
        getAnnotation(MyIsLakeClass.class);
        getAnnotation(String.class);
    }

    public static void getAnnotation(Class<?> cls) {
        try {
            IsLike annotation = cls.getAnnotation(IsLike.class);
            System.out.println("Значение аннотации: " + annotation.value());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
