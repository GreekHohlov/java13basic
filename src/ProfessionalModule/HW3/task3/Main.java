package ProfessionalModule.HW3.task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) {
        Class<APrinter> c = APrinter.class;

        try {
            Constructor<APrinter> constructor = c.getDeclaredConstructor();
            APrinter object = constructor.newInstance();
            object.print(50);
        } catch (NoSuchMethodException e) {
            System.out.println("Метод объекта не обнаружен! " + e.getMessage());
        } catch (InvocationTargetException e) {
            System.out.println("Вызываемый класс не найден! " + e.getMessage());
        } catch (InstantiationException e) {
            System.out.println("Невозможно создать объект класса! " + e.getMessage());
        } catch (IllegalAccessException e) {
            System.out.println("Доступ ограничен! " + e.getMessage());
        }
    }
}
