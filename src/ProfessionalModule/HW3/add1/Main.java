package ProfessionalModule.HW3.add1;
public class Main {
    public static void main(String[] args) {
        String s1 = "";
        String s2 = "(()()())";
        String s3 = ")(";
        String s4 = "(()";
        String s5 = "((()))";
        String s6 = "(";
        String s7 = "())";
        checkString(s1);
        checkString(s2);
        checkString(s3);
        checkString(s4);
        checkString(s5);
        checkString(s6);
        checkString(s7);
    }
    public static void checkString(String s){
        char openBracket = '(';
        char closeBracket = ')';
        if (s.length() == 0){
            System.out.println(true);
        } else if (s.charAt(0) == closeBracket || s.length() == 1) {
            System.out.println(false);
        } else {
          int openAmount = 1;
          int closeAmount = 0;
          for (int i = 1; i < s.length(); i++){
              if (s.charAt(i) == openBracket){
                  openAmount++;
              }
              if (s.charAt(i) == closeBracket){
                  closeAmount++;
              }
              if (closeAmount > openAmount){
                  break;
              }
          }
            System.out.println(openAmount == closeAmount);
          }
    }
}
