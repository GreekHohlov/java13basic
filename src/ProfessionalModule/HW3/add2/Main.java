package ProfessionalModule.HW3.add2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String s1 = "";
        String s2 = "{()[]()}";
        String s3 = "{)(}";
        String s4 = "[}";
        String s5 = "[{(){}}][()]{}";
        checkString(s1);
        checkString(s2);
        checkString(s3);
        checkString(s4);
        checkString(s5);
    }

    public static void checkString(String s) {
        char openBracketRound = '(';
        char openBracketCurly = '{';
        char openBracketSquare = '[';
        char closeBracketRound = ')';
        char closeBracketCurly = '}';
        char closeBracketSquare = ']';
        if (s.length() == 0) {
            System.out.println(true);
        } else if (s.charAt(0) == closeBracketRound || s.charAt(0) == closeBracketCurly ||
                s.charAt(0) == closeBracketSquare || s.length() == 1) {
            System.out.println(false);
        } else {
            List<Character> str = new ArrayList<>();
            for (int i = 0; i < s.length(); i++) {
                str.add(s.charAt(i));
            }
            int i = 1;
            while (str.size() > 0) {
                if (str.get(i) == closeBracketRound) {
                    if (str.get(i - 1) == openBracketRound) {
                        str.remove(i);
                        str.remove(i - 1);
                        i = 1;
                        continue;
                    } else {
                        break;
                    }
                }
                if (str.get(i) == closeBracketCurly) {
                    if (str.get(i - 1) == openBracketCurly) {
                        str.remove(i);
                        str.remove(i - 1);
                        i = 1;
                        continue;
                    } else {
                        break;
                    }
                }
                if (str.get(i) == closeBracketSquare) {
                    if (str.get(i - 1) == openBracketSquare) {
                        str.remove(i);
                        str.remove(i - 1);
                        i = 1;
                        continue;
                    } else {
                        break;
                    }
                }
                i++;
            }
            System.out.println(str.size() == 0);
        }
    }
}
