package ProfessionalModule.HW6.AddTask2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите целое число: ");
        int n = scanner.nextInt();

        System.out.println("Число простое? " + isSimple(n));
    }

    public static boolean isSimple(int n) {
        boolean flag = true;
        List<Integer> simple = new ArrayList<>();
        simple.add(2);
        simple.add(3);
        simple.add(5);
        simple.add(7);
        if (!simple.contains(Math.abs(n)) && n != 0) {
            for (int i = 0; i < simple.size(); i++) {
                if (n % simple.get(i) == 0) {
                    flag = false;
                    break;
                }
            }
        }
        return flag;
    }
}
