package ProfessionalModule.HW6.AddTask1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите натуральное число: ");
        int n = scanner.nextInt();
        if (n > 0 && n < 10) {
            System.out.println("Число Армстронга? " + true);
        }
        if (n <= 0) {
            System.out.println("Число Армстронга? " + false);
        } else {
            System.out.println("Число Армстронга? " + isArmstrong(n));
        }

    }

    public static boolean isArmstrong(int n) {
        List<Integer> intSet = new ArrayList<>();
        int temp = n;
        while (temp != 0) {
            intSet.add(temp % 10);
            temp /= 10;
        }
        Integer result = 0;
        for (Integer digit : intSet) {
            result += (int) (Math.pow(digit, intSet.size()));
        }
        return result.equals(n) && !result.equals(0);
    }
}
