/*
 Заполнение таблицы покупателей с указанием имени и номера телефона.
 */
insert into clients(name, phone)
values ('Иван', '89231234567');

insert into clients(name, phone)
values ('Татьяна', '89839514232');

insert into clients(name, phone)
values ('Павел', '89621597582');

insert into clients(name, phone)
values ('Вячеслав', '89234567812');

insert into clients(name, phone)
values ('Светлана', '89513694365');

insert into clients(name, phone)
values ('Дмитрий', '89834931725');

insert into clients(name, phone)
values ('Николай', '8137842113');