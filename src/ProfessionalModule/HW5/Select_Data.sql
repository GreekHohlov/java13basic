/*
1 По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ

 */
select o.id as "Номер заказа", f.article as "Название цветов", o.amount as "Количество", f.price as "Цена",
       o.amount * f.price as "Стоимость заказа", c.name as "Имя покупателя", c.phone as "Телефон"
from orders o join clients c on c.id = o.clients_id join flowers f on f.id = o.article_id
where o.id = 15;

/*
2 Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
 */
select c.id as "id клиента", c.name as "Имя", c.phone as "Телефон", f.article as "Цветы", o.amount as "Количество",
       o.amount * f.price as "Стоимость", o.date_orders as "Дата заказа"
from clients c join orders o on c.id = o.clients_id join flowers f on f.id = o.article_id
where c.id = 1 and o.date_orders >= now() - interval '30D' and o.date_orders <= now();

/*
3 Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
 */
select f.article as "Название", o.amount as "Количество"
from orders o join flowers f on f.id = o.article_id
where amount = (select max(amount) from orders);

/*
4 Вывести общую выручку (сумму золотых монет по всем заказам) за все
время

 */
select sum(o.amount * f.price) as "Общая выручка"
from orders o join flowers f on f.id = o.article_id;