/*
 Создание таблицы для хранения id, названия и стоимости цветка.
 */
create table flowers
(
    id      serial primary key,
    article varchar(50) not null,
    price   integer     not null
);

/*
 Создание таблицы для хранения id, имени покупателя и его номера телефона.
 */
create table clients
(
    id    serial primary key,
    name  varchar(50) not null,
    phone varchar(11) not null
);

/*
 Создание таблицы для хранения id заказа, id цветка из таблицы flowers, id покупателя из таблицы clients,
 количества цветов в заказе с ограничегием от 1 до 1000 и даты покупки.
 */
create table orders
(
    id          serial primary key,
    article_id  integer references flowers (id),
    clients_id  integer references clients (id),
    amount      integer   not null,
    check (amount > 0 and amount < 1000),
    date_orders timestamp not null
);