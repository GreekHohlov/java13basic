/*
 Заполнение таблицы заказов с указанием id цветов, id покупателя и даты покупки.
 */
insert into orders(article_id, clients_id, amount, date_orders)
values (1, 3, 5, now() - interval '75H');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 4, 13, now() - interval '1MONTH');

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 1, 25, now() - interval '1D');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 4, 7, now());

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 6, 14, now() - interval '35H');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 5, 17, now() - interval '14H');

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 7, 11, now() - interval '2D');

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 5, 37, '2022-11-15 08:15:23');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 2, 3, '2022-11-6 14:27:17');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 2, 41, '2022-11-5 10:43:36');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 4, 9, '2022-12-13 22:11:22');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 4, 101, '2023-01-01 06:33:49');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 3, 33, '2022-12-22 21:11:47');

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 6, 19, '2023-01-15 11:01:31');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 1, 25, '2022-12-25 17:47:59');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 6, 9, '2022-10-14 18:16:44');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 5, 21, '2022-12-26 13:22:14');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 2, 11, '2023-01-13 22:22:22');

insert into orders(article_id, clients_id, amount, date_orders)
values (3, 4, 55, '2022-12-28 09:33:43');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 6, 3, '2022-12-25 02:15:14');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 1, 23, '2022-11-15 16:01:33');

insert into orders(article_id, clients_id, amount, date_orders)
values (1, 3, 7, '2022-12-31 16:21:52');

insert into orders(article_id, clients_id, amount, date_orders)
values (2, 1, 13, '2022-12-31 06:17:11');