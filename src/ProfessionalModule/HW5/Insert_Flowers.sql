/*
 Заполнение таблицы цветов с указанием названия цветка и его цены.
 */
insert into flowers(article, price)
values ('Роза', '100');

insert into flowers(article, price)
values ('Лилия', '50');

insert into flowers(article, price)
values ('Ромашка', '25');